<?php
/*
 * Template Name: JSON Output
 * Description: JSON Output
 */

$projects = get_posts(array('post_type'=>'projects','posts_per_page'=>-1));
$jsonOutput = array();

foreach($projects as $project)
{

	$project_id = get_post_meta($project->ID, 'project_number', true);
	$project_permalink = get_permalink($project->ID);
	$jsonOutput[$project_id] = $project_permalink;
}
header('Content-Type: application/json');
echo json_encode($jsonOutput);
die();
