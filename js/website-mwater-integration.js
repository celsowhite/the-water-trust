var addBarChart, addPieChart, redrawFunctions, resizeTO;

window.displayMap = function(mapDiv, options) {
  var center, iconColors, infowindow, map, mapOptions, zoom;
  center = new google.maps.LatLng(4.4299457, 26.4545841);
  zoom = 13;
  mapOptions = {
    zoom: zoom,
    center: center,
    mapTypeId: google.maps.MapTypeId.HYBRID,
    scaleControl: true
  };
  map = new google.maps.Map(mapDiv, mapOptions);
  infowindow = new google.maps.InfoWindow();
  iconColors = [options.imagePath + 'inadequateMarker.png', options.imagePath + 'basicMarker.png', options.imagePath + 'excellentMarker.png', options.imagePath + 'nodataMarker.png'];
  return jQuery.getJSON("https://api.mwater.co/v3/queries/twt-map.json", (function(_this) {
    return function(sites) {
      var withPages;
      withPages = function(pages) {
        var addMarker, bounds, iconIndex, latLng, marker, site, status, _i, _len;
        bounds = new google.maps.LatLngBounds();
        for (_i = 0, _len = sites.length; _i < _len; _i++) {
          site = sites[_i];
          latLng = new google.maps.LatLng(site.lat, site.lng);
          bounds.extend(latLng);
          iconIndex = (function() {
            switch (site.functionality) {
              case 'low':
                return 0;
              case 'medium':
                return 1;
              case 'high':
                return 2;
              default:
                return status = 3;
            }
          })();
          marker = new google.maps.Marker({
            position: latLng,
            title: site.name,
            map: map,
            icon: iconColors[iconIndex]
          });
          addMarker = function(site, marker) {
            return google.maps.event.addListener(marker, 'click', function() {
              var content;
              content = '<div style="line-height:1.35;overflow:hidden;white-space:nowrap;">';
              if (site.photo != null) {
                content += "<img src='https://api.mwater.co/v3/images/" + site.photo + "?h=150' ><br>";
              }
              switch (site.functionality) {
                case 'low':
                  status = "Not functional";
                  break;
                case 'medium':
                  status = "Partially functional but in need of repair";
                  break;
                case 'high':
                  status = "Functional";
                  break;
                default:
                  status = "No functionality data";
              }
              content += 'Project name: <b>' + site.name + '</b>';
              content += "<br>Water source type: <b>" + site.type + '</b>';
              content += "<br>Status: <b>" + status + '</b>';
              if (pages && pages[site.twtid]) {
                content += '<br><br><a href="' + pages[site.twtid] + '">Open Project Page</a>';
              }
              content += "</div>";
              infowindow.setContent(content);
              return infowindow.open(map, marker);
            });
          };
          addMarker(site, marker);
        }
        return map.fitBounds(bounds);
      };
      return jQuery.getJSON("/projects-json/").done(withPages).fail(function() {
        return withPages();
      });
    };
  })(this));
};

redrawFunctions = [];

resizeTO = null;

jQuery(window).resize(function() {
  if (resizeTO) {
    clearTimeout(resizeTO);
  }
  return resizeTO = setTimeout(function() {
    return jQuery(this).trigger('resizeEnd');
  }, 500);
});

jQuery(window).on('resizeEnd', function() {
  var redrawFunction, _i, _len, _results;
  _results = [];
  for (_i = 0, _len = redrawFunctions.length; _i < _len; _i++) {
    redrawFunction = redrawFunctions[_i];
    _results.push(redrawFunction());
  }
  return _results;
});

addPieChart = function(div, field, levels, showLegend, indicators) {
  var chart, colors, dataTable, i, indicator, level, options, redrawFunction, value, _i;
  options = {
    pieHole: 0.40,
    width: '100%',
    height: '100%',
    chartArea: {
      left: '3%',
      top: '3%',
      width: '94%',
      height: '94%'
    },
    tooltip: {isHtml: true},
    pieSliceTextStyle: {
            color: 'black',
            fontSize: '14',
    },
  };
  dataTable = new google.visualization.DataTable();
  dataTable.addColumn('string', 'indicators');
  dataTable.addColumn('number', 'occurences');
  colors = [];
  for (i = _i = 0; _i < 4; i = ++_i) {
    indicator = indicators[i];
    level = levels[i];
    value = indicator[field];
    if (value === 0) {
      continue;
    }
    dataTable.addRow([level[0], value]);
    colors.push(level[1]);
  }
  options.colors = colors;
  chart = new google.visualization.PieChart(div);
  if (!showLegend) {
    options.legend = 'none';
  }
  redrawFunction = function() {
    return chart.draw(dataTable, options);
  };
  redrawFunctions.push(redrawFunction);
  return redrawFunction();
};

window.displayIndicatorDonut = function(placements, levels) {
  return google.load("visualization", "1", {
    packages: ["corechart"],
    callback: (function(_this) {
      return function() {
        return jQuery.getJSON("https://api.mwater.co/v3/queries/twt-indicators.json", function(indicators) {
          var div, field, placement, showLegend, _i, _len, _results;
          indicators[1].water_quality += indicators[2].water_quality;
          indicators[2].water_quality = 0;
          _results = [];
          for (_i = 0, _len = placements.length; _i < _len; _i++) {
            placement = placements[_i];
            console.log('processing placement: ' + placement);
            div = placement[0];
            field = placement[1];
            showLegend = placement[2];
            console.log(div);
            _results.push(addPieChart(div, field, levels, showLegend, indicators));
          }
          return _results;
        });
      };
    })(this)
  });
};

addBarChart = function(div, field, indicators, levels, height) {
  var chart, dataColumns, dataTable, dataValues, indicator, levelIndex, options, redrawFunction, _i, _ref;
  dataColumns = ['Indicator'];
  dataValues = [''];
  for (levelIndex = _i = 0, _ref = levels.length; 0 <= _ref ? _i < _ref : _i > _ref; levelIndex = 0 <= _ref ? ++_i : --_i) {
    indicator = indicators[levelIndex];
    if (indicator[field] === 0) {
      continue;
    }
    dataColumns.push(levels[levelIndex][0]);
    dataColumns.push({
      role: 'style'
    });
    dataValues.push(indicator[field]);
    dataValues.push(levels[levelIndex][1]);
  }
  dataTable = google.visualization.arrayToDataTable([dataColumns, dataValues]);
  options = {
    height: height,
    legend: 'none',
    isStacked: true,
    axisTitlesPosition: 'none',
    tooltip: {isHtml: true},
    vAxis: {
      textPosition: 'none',
      gridlines: {
        color: 'transparent'
      },
      baselineColor: 'transparent'
    },
    hAxis: {
      textPosition: 'none',
      gridlines: {
        color: 'transparent'
      },
      baselineColor: 'transparent'
    }
  };
  chart = new google.visualization.BarChart(div);
  redrawFunction = function() {
    return chart.draw(dataTable, options);
  };
  redrawFunctions.push(redrawFunction);
  return redrawFunction();
};

window.displayHorizontalBarIndicator = function(placements, levels, height, twtid) {
  return google.load("visualization", "1", {
    packages: ["corechart"],
    callback: (function(_this) {
      return function() {
        return jQuery.getJSON("https://api.mwater.co/v3/queries/twt-indicators.json", {
          twtid: twtid
        }, function(indicators) {
          var div, field, i, placement, total, _i, _j, _len, _results;
          indicators[1].water_quality += indicators[2].water_quality;
          indicators[2].water_quality = 0;
          _results = [];
          for (_i = 0, _len = placements.length; _i < _len; _i++) {
            placement = placements[_i];
            div = placement[0];
            field = placement[1];
            total = 0;
            for (i = _j = 0; _j < 4; i = ++_j) {
              total += indicators[i][field];
            }
            if (total === 0) {
              indicators[3][field] = 1;
            }
            _results.push(addBarChart(div, field, indicators, levels, height));
          }
          return _results;
        });
      };
    })(this)
  });
};
