<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}


if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider">
				<div class="q_slider_inner">
					<?php echo do_shortcode($revslider); ?>
				</div>
			</div>
		<?php
		}
		?>
		<div class="container"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
			<div class="container_inner default_template_holder clearfix page_container_inner">
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>
					<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>

							<div class="two_columns_66_33 clearfix portfolio_container">
								<div class="column1">
									<div class="column_inner">

										<!-- PROJECT DESCRIPTION -->

										<div class="project_header">
											<h1><?php the_title(); ?></h1>
											<h3>Donor: <?php the_field('donor_name'); ?></h3>
										</div> 

										<div><?php the_field('project_information'); ?></div> 

										<!-- Construction Updates (not required) -->

										<?php if( get_field('construction_updates') ): ?>

										<div data-border-radius="" data-collapsible="no" data-active-tab="false" class="q_accordion_holder clearfix wpb_content_element toggle boxed  not-column-inherit accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset" style="visibility: visible;">
											<h5 style="" class="clearfix title-holder ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom">
												<span class="accordion_mark left_mark"><span class="accordion_mark_icon"></span></span>
												<span class="tab-title">View Construction Update</span><span class="accordion_mark right_mark">
												<span class="accordion_mark_icon"></span></span>
											</h5>
											<div class="accordion_content no_icon ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: none;">
												<div class="accordion_content_inner"><?php the_field('construction_updates'); ?></div>
											</div>
										</div>

									    <?php endif; ?>

									    <!-- Sanitation and Hygiene Strategy (not required) -->

										<?php if( get_field('sanitation_hygiene_strategy') ): ?>

										<div data-border-radius="" data-collapsible="no" data-active-tab="false" class="q_accordion_holder clearfix wpb_content_element toggle boxed  not-column-inherit accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset" style="visibility: visible;">
											<h5 style="" class="clearfix title-holder ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom">
												<span class="accordion_mark left_mark"><span class="accordion_mark_icon"></span></span>
												<span class="tab-title">View Sanitation and Hygiene Strategy</span><span class="accordion_mark right_mark">
												<span class="accordion_mark_icon"></span></span>
											</h5>
											<div class="accordion_content no_icon ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: none;">
												<div class="accordion_content_inner"><?php the_field('sanitation_hygiene_strategy'); ?></div>
											</div>
										</div>

									    <?php endif; ?>

									    <!-- Technical Data (not required) -->

										<?php if( get_field('technical_data') ): ?>

										<div data-border-radius="" data-collapsible="no" data-active-tab="false" class="q_accordion_holder clearfix wpb_content_element toggle boxed  not-column-inherit accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset" style="visibility: visible;">
											<h5 style="" class="clearfix title-holder ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom">
												<span class="accordion_mark left_mark"><span class="accordion_mark_icon"></span></span>
												<span class="tab-title">View Technical Data</span><span class="accordion_mark right_mark">
												<span class="accordion_mark_icon"></span></span>
											</h5>
											<div class="accordion_content no_icon ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: none;">
												<div class="accordion_content_inner"><?php the_field('technical_data'); ?></div>
											</div>
										</div>

									    <?php endif; ?>

									    <!-- PHOTOS (IMAGE GRID AND FLICKR) -->

										<h2 class="secondary_header">Photos</h2>

										<!-- If photos are uploaded or a flickr gallery inserted then show -->

										<?php if( get_field('photos') || get_field('flickr')): ?>

										   <?php $images = get_field( 'photos' ); if( $images ): ?>

										      <?php foreach( $images as $image ): ?>

											      <?php $image_large = wp_get_attachment_image_src($image['id'], 'full'); ?>
											      <?php $image_thb = wp_get_attachment_image_src($image['id'], 'thumbnail'); ?>
											      <a href="<?php echo $image_large[0]; ?>" rel="lightbox[photos]">
												     <img src="<?php echo $image_thb[0]; ?>" class="attachment-thumbnail" />
											      </a>
									          
									          <?php endforeach; ?>

								            <?php endif; ?>

								            <!-- Flickr Gallery -->

											<?php if( get_field('flickr') ): ?>

												<?php if( have_rows('flickr') ): ?>

													<?php while ( have_rows('flickr') ) : the_row();?>

														<div class="embed-container">
															<?php the_sub_field('flickr_gallery'); ?>
														</div>
													
													<?php endwhile; ?>

												<?php endif; ?>

											<?php endif; ?>

											<!-- If no photos or flickr gallery are inserted then display Coming Soon -->

											<?php else: ?>
											
											<p>Coming Soon</p>

						                <?php endif; ?>

									    <!-- YOUTUBE VIDEO EMBEDS -->

										<h2 class="secondary_header">Videos</h2>

										<!-- If videos were entered then show -->

										<?php if( get_field('videos') ): ?>

											<?php if( have_rows('videos') ): ?>

												<?php while ( have_rows('videos') ) : the_row();?>

													<div class="wpb_video_widget wpb_content_element">
														<div class="wpb_wrapper">
															<div class="wpb_video_wrapper project_video_player">
																<?php the_sub_field('video');?>
															</div>
														</div>
													</div>
												
												<?php endwhile; ?>

											<?php endif; ?>

										<!-- If no videos were entered -->
										    <?php else :?>
											
											<p>Coming Soon</p>

										<?php endif; ?>

	                                </div> <!-- End Column Inner -->
	                            </div> <!-- End Column 1 -->

	                            <!-- Start Column 2 -->

	                            <div class="column2">
	                            	<div class="column_inner">
	                            		<!-- Featured Image -->
	                            		<?php if ( has_post_thumbnail() ) { ?>
	                            		<div class="post_image">	                           			
	                            				<?php the_post_thumbnail('full'); ?>
	                            		</div>
	                            		<?php } ?>

	                            		<!-- Project Details -->
	                            		<h2 class="secondary_header">Project Details</h2>

	                            		<!-- Project Location -->
	                            		<?php if( get_field('project_location') ): ?>
											<p><b>Location:</b> <?php the_field('project_location'); ?></p>
										<?php endif; ?>

										<!-- Catchment Population -->
										<?php if( get_field('catchment_population')): ?>
	                            			<p><b>Catchment Population:</b> <?php the_field('catchment_population'); ?></p>
	                            		<?php endif; ?>

	                            		<!-- Project Start Date -->
	                            		<?php if( get_field('start_date')): ?>
	                            			<p><b>Start Date:</b> <?php the_field('start_date'); ?></p>
	                            		<?php endif; ?>

	                            		<!-- If a completion date was entered then show the date -->
	                            		<?php if( get_field('installation_completion_date') ): ?>

	                            			<!-- If Pump was selected from the dropdown then show the pump installation date -->
		                            		<?php if( get_field('project_date_type') == "pump_date"): ?>
		                            			<p><b>Pump Installation Date:</b> <?php the_field('installation_completion_date'); ?></p>
		                            		<?php endif; ?>

		                            		<!-- If a school project was selected from the dropdown then show school completion date -->
		                            		<?php if( get_field('project_date_type') == "school_date"): ?>
		                            			<p><b>School Project Completion Date:</b> <?php the_field('installation_completion_date'); ?></p>
		                            		<?php endif; ?>

		                            	<?php endif; ?>

	                            		<!-- mWater Data -->
	                            		<h2 class="secondary_header">Key Performance Indicators</h2>

	                            		<p>The following data will be collected approximately 6 to 8 months after the installation of the water point with the start of our monitoring program.</p>

										<h5>Water Quality</h5>
	                            		<div id="first_bar" class="project_indicator"></div>
										
										<h5>Functionality</h5>
	                            		<div id="second_bar" class="project_indicator"></div>

	                            		<h5>Latrine Use</h5>
	                            		<div id="third_bar" class="project_indicator"></div>

	                            		<div class="projects_legend legend">
											<div class="legend-item">
												<div class="legend-box high"></div>
												<div class="legend-text">High</div>
											</div>
											<div class="legend-item">
												<div class="legend-box basic"></div>
												<div class="legend-text">Basic</div>
											</div>
											<div class="legend-item">
												<div class="legend-box inadequate"></div>
												<div class="legend-text">Inadequate</div>
											</div>
											<div class="legend-item">
												<div class="legend-box nodata"></div>
												<div class="legend-text">No Data</div>
											</div>
										</div>
										<p>We are committed to long-lasting change and to get there, we measure key performance indicators of all our projects.  Read about them <a href="http://watertrust.org/key-performance-indicators/">here</a>.  See them <a href="http://watertrust.org/our-projects/">here</a>. We are committed to clean, safe water.  To get there, we will do <a href="http://watertrust.org/our-commitment-to-clean-water/">this</a>.</p>


	                            		<script>
												jQuery(function() {
													window.displayHorizontalBarIndicator([
														[jQuery("#first_bar").get(0), "water_quality"],
														[jQuery("#second_bar").get(0), "functionality"],
														[jQuery("#third_bar").get(0), "uses_latrine"]
														],
														[
														["Inadequate", '#939598'],
														['Basic', '#272962'],
														['High', '#378BCA'],
														["In December 2014, we implemented a mobile monitoring system across all our projects that give real-time data and shows exactly where our impact is.  Thanks for your patience as we collect our data.", '#DCDDDE']
														],
														50,
														<?php the_field('project_number') ?>
														);
												 })
										</script>

	                            	</div>
	                            </div> <!-- End Column 2 -->
	                        </div> <!-- End Two Column layout -->
	                        
							<?php 
								$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
								);
								wp_link_pages($args_pages);
							?>
							<?php
							if($enable_page_comments){
								comments_template('', true); 
							}
							?> 
							<?php endwhile; ?>
						<?php endif; ?>
				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
					
					<?php if($sidebar == "1") : ?>	
						<div class="two_columns_66_33 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php elseif($sidebar == "2") : ?>	
						<div class="two_columns_75_25 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php endif; ?>
							<?php if (have_posts()) : 
								while (have_posts()) : the_post(); ?>
								<div class="column_inner">
								
								<?php the_content(); ?>
								<?php 
									$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
									);

									wp_link_pages($args_pages);
								?>
								<?php
								if($enable_page_comments){
									comments_template('', true); 
								}
								?> 
								</div>
						<?php endwhile; ?>
						<?php endif; ?>
					
									
							</div>
							<div class="column2"><?php get_sidebar();?></div>
						</div>
					<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
						<?php if($sidebar == "3") : ?>	
							<div class="two_columns_33_66 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php elseif($sidebar == "4") : ?>	
							<div class="two_columns_25_75 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php endif; ?>
								<?php if (have_posts()) : 
									while (have_posts()) : the_post(); ?>
									<div class="column_inner">
										<?php the_content(); ?>
										<?php 
											$args_pages = array(
												'before'           => '<p class="single_links_pages">',
												'after'            => '</p>',
												'pagelink'         => '<span>%</span>'
											);
											wp_link_pages($args_pages);
										?>
										<?php
										if($enable_page_comments){
											comments_template('', true); 
										}
										?> 
									</div>
							<?php endwhile; ?>
							<?php endif; ?>

						
										
								</div>
								
							</div>
					<?php endif; ?>

			
		</div>
	</div>
	<?php get_footer(); ?>