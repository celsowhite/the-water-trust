<?php

/****************************************
* Load Scripts and Styles
****************************************/

function twt_load_files(){

	// Load Lazy Load Script for Home and About pages with Video Player

	if ( is_front_page() || is_page(16016) ) {

	    wp_register_script('add-lazyload-js', get_stylesheet_directory_uri() . '/js/lazyload.js', array('jquery'),'', true );
	    wp_enqueue_script('add-lazyload-js');
	}

	// Load Scripts for mWater

	if ( is_page(16773) || is_page(16016) || is_singular( 'projects' ) ) {

	    wp_register_script('add-googlejavascriptapi-js', 'https://www.google.com/jsapi', '','', true );
	    wp_enqueue_script('add-googlejavascriptapi-js');

	    wp_register_script('add-googlemaps-js', 'http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false', '','', true );
	    wp_enqueue_script('add-googlemaps-js');

	    wp_register_script('add-mwater-js', get_stylesheet_directory_uri() . '/js/website-mwater-integration.js', array('jquery'),'', true );
	    wp_enqueue_script('add-mwater-js');

	}

	// Load Child Theme CSS

	wp_register_style( 'child_style', get_stylesheet_directory_uri() . '/style.css'  );
	wp_enqueue_style( 'child_style' );

	// Load Sentinel font from cloud typography

	wp_register_style('sentinel_font', '//cloud.typography.com/7833332/746006/css/fonts.css', '','', '' );
    wp_enqueue_style('sentinel_font');

}

add_action('wp_enqueue_scripts', 'twt_load_files', 13); 

//Making jQuery to load from Google Library
function replace_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3');
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'replace_jquery');

/****************************************
* Custom Excerpt Length
****************************************/

function get_excerpt($count){

  $permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = $excerpt.'...';
  return $excerpt;
  
}

/****************************************
* Custom Post Types
****************************************/

// Create Projects Custom Post Type

function custom_post_type_projects() {

	$labels = array(
		'name'                => _x( 'Projects', 'Post Type General Name'),
		'singular_name'       => _x( 'Project', 'Post Type Singular Name'),
		'menu_name'           => __( 'Projects'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Projects'),
		'view_item'           => __( 'View Project'),
		'add_new_item'        => __( 'Add New Project'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Project'),
		'update_item'         => __( 'Update Project'),
		'search_items'        => __( 'Search Projects'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);
	
	$args = array(
		'label'               => __( 'project'),
		'description'         => __( 'The Water Trust projects'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'page-attributes', 'custom-fields',  ),
		'hierarchical'        => true,
		'taxonomies'          => array( 'project_category' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-location-alt',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	register_post_type( 'projects', $args );

}

add_action( 'init', 'custom_post_type_projects', 0 );

// Create Jobs Custom Post Type

function custom_post_type_jobs() {

	$labels = array(
		'name'                => _x( 'Jobs', 'Post Type General Name'),
		'singular_name'       => _x( 'Job', 'Post Type Singular Name'),
		'menu_name'           => __( 'Jobs'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Jobs'),
		'view_item'           => __( 'View Job'),
		'add_new_item'        => __( 'Add New Job'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Job'),
		'update_item'         => __( 'Update Job'),
		'search_items'        => __( 'Search Job'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);
	
	$args = array(
		'label'               => __( 'job'),
		'description'         => __( 'The Water Trust jobs'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt',  ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-id-alt',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	register_post_type( 'job', $args );

}

add_action( 'init', 'custom_post_type_jobs', 0 );


/****************************************
* Custom Taxonomies
****************************************/

// Projects Custom Taxonomy

function add_project_taxonomies() {

  register_taxonomy('project_category', 'projects', array(

      'hierarchical' => true,
      'labels' => array(
      'name' => _x( 'Project Categories', 'taxonomy general name' ),
      'singular_name' => _x( 'Project Category', 'taxonomy singular name' ),
      'search_items' =>  __( 'Project Categories' ),
      'all_items' => __( 'Project Categories' ),
      'parent_item' => __( 'Parent Category' ),
      'parent_item_colon' => __( 'Parent Category:' ),
      'edit_item' => __( 'Edit Category' ),
      'update_item' => __( 'Update Category' ),
      'add_new_item' => __( 'Add New Category' ),
      'new_item_name' => __( 'New Category Name' ),
      'menu_name' => __( 'Project Categories' ),
      'separate_items_with_commas' => __('Yo'),
    ),

      'rewrite' => array(
      'slug' => 'project-category', 
      'with_front' => false,
      'hierarchical' => false 
    ),
  ));
}

add_action( 'init', 'add_project_taxonomies', 0 );


/****************************************
* Customize the Featured Image metabox for 
  'Projects' custom post type w/ info-text
****************************************/

function mim_remove_featuredimage_metabox() {
    remove_meta_box( 'postimagediv', 'projects', 'side' );
}

add_action( 'admin_head', 'mim_remove_featuredimage_metabox' );
 
function mim_custom_featuredimage_metabox( $post ) {
	
	$thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true );
	
	echo "For TWT HQ use. The Featured Image is the image that will appear as the thumbnail for the project across the site and in the upper right hand corner of each project page.";
	echo _wp_post_thumbnail_html( $thumbnail_id, $post->ID );
	
}

function mim_add_featuredimage_metabox() {
	add_meta_box( 'postimagediv', __( 'Featured Image' ), 'mim_custom_featuredimage_metabox', 'projects', 'side', 'default' );
}

add_action( 'admin_head', 'mim_add_featuredimage_metabox' );


/****************************************
* Add custom meta to project json request
****************************************/

function custom_json_api_prepare_post( $post_response, $post, $context ) {
 
    $meta = get_post_meta( $post['ID'] );
    $post_response[acf_project_fields] = $meta;
 
    return $post_response;
}
add_filter( 'json_prepare_post', 'custom_json_api_prepare_post', 10, 3 );


/****************************************
* Overwrite sub-directory files in 
  child theme
****************************************/

//Overwrite custom post types created by Bridge theme

require_once( get_stylesheet_directory() . '/includes/qode-custom-post-types.php' );

/****************************************
* Custom Filter for Plugins
****************************************/

// Change WP Menu Cart so only a number appears after the cart icon

add_filter('wpmenucart_menu_item_a_content', 'adjust_wpmenucart_item_a_content', 12, 4);
function adjust_wpmenucart_item_a_content ($menu_item_a_content, $menu_item_icon, $cart_contents, $item_data) {

	$item_data = $item_data;
	$cart_contents = $item_data['cart_contents_count'];
	$menu_item_icon = '<i class="wpmenucart-icon-shopping-cart-'.$icon.'"></i>';
	$menu_item_a_content .= '<span class="cartcontents">'.$cart_contents.'</span>';
	
	return $menu_item_a_content;

}

// Change number of products displayed in the shop

add_filter('loop_shop_per_page', create_function('$cols', 'return 24;'), 21);

// Move Yoast to bottom

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

function yoasttobottom() {
	return 'low';
}

/****************************************
* Add Comment to Paypal Pro Submission
****************************************/

// Comments for One-Time Donation Pay Pal Payments Pro

add_filter('gform_paypalpaymentspro_args_before_payment','add_to_paypal_payments_pro', 10, 2 );
function add_to_paypal_payments_pro($args, $form_id) {

	if ($form_id == 1) {
	$form = GFFormsModel::get_form_meta($form_id);

	$args['custom'] .= $form['title'];

	}

	if ($form_id == 2) {
	$form = GFFormsModel::get_form_meta($form_id);

	$args['custom'] .= $form['title'];

	}

	return $args;
}

// Comments for Subscription Donation Pay Pal Payments Pro

add_filter('gform_paypalpaymentspro_args_before_subscription', 'add_to_paypal_payments_pro_subscription', 10, 2 );
function add_to_paypal_payments_pro_subscription( $subscription, $form_id ){

// apply to form 1 only
if ( 1 == $form_id ) {
$form = GFFormsModel::get_form_meta($form_id);
$subscription['custom'] .= $form['title'];
}

// apply to form 2 only
if ( 2 == $form_id ) {
$form = GFFormsModel::get_form_meta($form_id);
$subscription['custom'] .= $form['title'];
}

// always return the subscription, even if not changing anything
return $subscription;
}

// Comments for PayPal Standard Donation

add_filter('gform_paypal_query', 'add_to_paypal_query', 10, 3);

function add_to_paypal_query($query_string, $form, $entry) {
	if ($form['id'] == 1) {
		parse_str(ltrim($query_string, '&'), $query);

		$query['custom'] .= $form['title'];
		// or you could just set it like this to whatever field 7 in the form was
		// $query['item_name_1'] = $entry['7'];
		$query_string = '&' . http_build_query($query);
	}
	if ($form['id'] == 2) {
		parse_str(ltrim($query_string, '&'), $query);

		$query['custom'] .= $form['title'];
		// or you could just set it like this to whatever field 7 in the form was
		// $query['item_name_1'] = $entry['7'];
		$query_string = '&' . http_build_query($query);
	}
	return $query_string;
}

// Comments for One-Time Donation Only to manager.paypal.com

/*=== add_filter( 'gform_paypalpaymentspro_args_before_payment','add_donation_comment', 10, 2 );
function add_donation_comment( $args, $form_id ) {
	// apply to form 1 only
	if ( 1 == $form_id ) {
		$args["COMMENT1"] = 'Field Project';
	}
	// apply to form 2 only
	if ( 2 == $form_id ) {
		$args["COMMENT1"] = 'Help Us Grow';
	}
	// always return the args, even if not changing anything 
	return $args;
}===*/

/****************************************
* Automatic Emails when Projects are posted
****************************************/

/*
 * Draft to Under Construction Project Hook
*/

function draft_to_under_construction_project( $new_status, $old_status, $post ) {

	if ($post->post_type == 'projects' && $post->post_parent == 615 && $old_status == 'draft' && $new_status == 'publish') {

		$multiple_recipients = array(
		    'communication@watertrust.org',
			'lgarciapuig@watertrust.org',
			'ldioguardi@watertrust.org',
			'admin@watertrust.org'
		);
		$subject = "New Project Posted";
		$title = $post->post_title;
		$link = get_post_permalink($post->ID);
		$message = 'A new project, ' . $title . ', has moved from draft to under constuction. View the project here ' . $link;
		$headers = 'From: The Water Trust <admin@watertrust.org>';
		wp_mail( $multiple_recipients, $subject, $message, $headers );
	}

}

add_action('transition_post_status', 'draft_to_under_construction_project', 10, 3);

/*
 * Draft to Complete Project Hook
*/

function draft_to_published_project( $new_status, $old_status, $post ) {

	if ($post->post_type == 'projects' && $post->post_parent !== 615 && $old_status == 'draft' && $new_status == 'publish') {
		$multiple_recipients = array(
		    'communication@watertrust.org',
			'lgarciapuig@watertrust.org',
			'ldioguardi@watertrust.org',
			'admin@watertrust.org'
		);
		$subject = "New Project Posted";
		$title = $post->post_title;
		$link = get_post_permalink($post->ID);
		$message = 'A new project, ' . $title . ', has been published. View the project here ' . $link;
		$headers = 'From: The Water Trust <admin@watertrust.org>';
		wp_mail( $multiple_recipients, $subject, $message, $headers );
	}

}

add_action('transition_post_status', 'draft_to_published_project', 10, 3);

/*
 * Under Construction to Complete Hook
*/

function under_construction_to_complete($post_ID, $post_after, $post_before) {

	if ($post_before->post_parent == 615 && ($post_after->post_parent == 613 || $post_after->post_parent == 732)) {
		$multiple_recipients = array(
		    'communication@watertrust.org',
			'lgarciapuig@watertrust.org',
			'ldioguardi@watertrust.org',
			'admin@watertrust.org'
		);
		$subject = "Project Complete";
		$title = $post_after->post_title;
		$link = get_post_permalink($post_ID);
		$message = 'The project ' . $title . ' has moved from under construction to complete. View the project here ' . $link;
		$headers = 'From: The Water Trust <admin@watertrust.org>';
		wp_mail( $multiple_recipients, $subject, $message, $headers );
	}

}

add_action('post_updated', 'under_construction_to_complete', 10, 3);