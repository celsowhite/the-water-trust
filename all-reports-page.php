<?php 
/*
Template Name: All Reports Page
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider">
				<div class="q_slider_inner">
					<?php echo do_shortcode($revslider); ?>
				</div>
			</div>
		<?php
		}
		?>
		<div class="container"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
			<div class="container_inner default_template_holder clearfix page_container_inner">
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>
					<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							
							<div style="padding-top:0px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section subtle_background_pattern">
								<div class=" section_inner clearfix">
									<div class="section_inner_margin clearfix">
										<div class="vc_col-sm-12 wpb_column vc_column_container">
											<div class="wpb_wrapper">
												<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
													<div class=" full_section_inner clearfix">

														<!-- Field Reports -->

														<div class="vc_col-sm-12 wpb_column vc_column_container">
															<h2 class="secondary_header">Field Reports</h2>
															
															<?php if( have_rows('fieldreports', 16570) ): ?>

																	<ul class="reports_list_full">
																		<?php while ( have_rows('fieldreports', 16570) ) : the_row();?>
																			<li><a href="<?php the_sub_field('field_report_file')?>" target="_blank"><?php the_sub_field('field_report_name');?></a></li>
																		<?php endwhile; ?>
																	</ul>		

															<?php endif; ?>	 
														 </div>

														<!-- Annual Reports -->

														<div class="vc_col-sm-12 wpb_column vc_column_container">
																<h2 class="secondary_header">Annual Reports</h2>
																<?php if( have_rows('annualreports', 16570) ): ?>
																	<ul class="reports_list_full">
																		<?php while ( have_rows('annualreports', 16570) ) : the_row();?>
																			<li><a href="<?php the_sub_field('annual_report_file')?>" target="_blank"><?php the_sub_field('annual_report_name');?></a></li>
																		<?php endwhile; ?>
																	</ul>
																<?php endif; ?>
															</div> 
														</div> 

														<!-- Financial Reports -->

														<div class="vc_col-sm-12 wpb_column vc_column_container">
															<div class="wpb_wrapper">
																<h2 class="secondary_header">Financial Reports</h2>
																<?php if( have_rows('financialreports', 16570) ): ?>
																	<ul class="reports_list_full">
																		<?php while ( have_rows('financialreports', 16570) ) : the_row();?>
																			<li><a href="<?php the_sub_field('financial_report_file')?>" target="_blank"><?php the_sub_field('financial_report_name');?></a></li>
																		<?php endwhile; ?>
																	</ul>
																<?php endif; ?>
															</div> 
														</div>

														<!-- Note About Reports posted prior to April 2012 --> 

														<div class="vc_col-sm-12 wpb_column vc_column_container">
															<div class="wpb_wrapper">
															<p style="font-size: 12px; margin-top: 20px;">*Note that documents prior to April 2012 refer to The Water Trust’s field office in Uganda as “Busoga Trust in Masindi”</p>
															</div> 
														</div> 

													</div>
												</div>
											</div> 
										</div> 
									</div>
								</div>
							</div>

							<?php 
								$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
								);
								wp_link_pages($args_pages);
							?>
							<?php
							if($enable_page_comments){
								comments_template('', true); 
							}
							?> 
							<?php endwhile; ?>
						<?php endif; ?>
				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
					
					<?php if($sidebar == "1") : ?>	
						<div class="two_columns_66_33 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php elseif($sidebar == "2") : ?>	
						<div class="two_columns_75_25 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php endif; ?>
							<?php if (have_posts()) : 
								while (have_posts()) : the_post(); ?>
								<div class="column_inner">
								
								<?php the_content(); ?>
								<?php 
									$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
									);

									wp_link_pages($args_pages);
								?>
								<?php
								if($enable_page_comments){
									comments_template('', true); 
								}
								?> 
								</div>
						<?php endwhile; ?>
						<?php endif; ?>
					
									
							</div>
							<div class="column2"><?php get_sidebar();?></div>
						</div>
					<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
						<?php if($sidebar == "3") : ?>	
							<div class="two_columns_33_66 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php elseif($sidebar == "4") : ?>	
							<div class="two_columns_25_75 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php endif; ?>
								<?php if (have_posts()) : 
									while (have_posts()) : the_post(); ?>
									<div class="column_inner">
										<?php the_content(); ?>
										<?php 
											$args_pages = array(
												'before'           => '<p class="single_links_pages">',
												'after'            => '</p>',
												'pagelink'         => '<span>%</span>'
											);
											wp_link_pages($args_pages);
										?>
										<?php
										if($enable_page_comments){
											comments_template('', true); 
										}
										?> 
									</div>
							<?php endwhile; ?>
							<?php endif; ?>
						
										
								</div>
								
							</div>
					<?php endif; ?>
			
		</div>
	</div>
	<?php get_footer(); ?>