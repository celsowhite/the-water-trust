<?php 
/*
Template Name: Donate Page
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider"><div class="q_slider_inner">
			<?php echo do_shortcode($revslider); ?>
			</div></div>
		<?php
		}
		?>
	<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
	<div class="full_width_inner">
		<?php if(($sidebar == "default")||($sidebar == "")) : ?>
			<?php if (have_posts()) : 
					while (have_posts()) : the_post(); ?>

					<!-- Donation Methods -->
					
					<div style="background-color:#ffffff; padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section" data-q_icon="fa-list" data-q_title="Portfolio List" data-q_id="#portfolio">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-6 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('help_us_grow'); ?>
													</div> 
												</div> 

												<div class="vc_col-sm-6 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donate_to_a_field_project'); ?>
													</div> 
												</div> 
											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<!-- Donation Levels -->

					<div style="padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section subtle_background_pattern">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<?php the_field('donation_levels_header'); ?>
										<div style=" text-align:left; padding-bottom: 40px;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donation_level_1'); ?>
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donation_level_2'); ?>
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donation_level_3'); ?>
													</div> 
												</div> 
											</div>
										</div>
										<div style=" text-align:left; padding-bottom: 40px;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donation_level_4'); ?>
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donation_level_5'); ?>
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donation_level_6'); ?>
													</div> 
												</div> 
											</div>
										</div>
										<div style=" text-align:left; padding-bottom: 0px;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('donation_level_7'); ?>
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
													</div> 
												</div> 
											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<!-- Full Width Image -->

					<div style="background-image:url(http://watertrust.org/wp-content/uploads/2014/10/donate_bottom.jpg); padding-top:100px; padding-bottom:100px; text-align:center;" class="vc_row wpb_row section vc_row-fluid grid_section">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">


									</div> 
								</div> 
							</div>
						</div>
					</div>

					<!-- Donate How-To and Address Information -->
					
					<div style="padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section subtle_background_pattern" data-q_icon="fa-list" data-q_title="Portfolio List" data-q_id="#portfolio">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-8 wpb_column vc_column_container">
									<div class="wpb_wrapper">
									<?php the_field('how_to_donate'); ?>
									</div> 
								</div> 

								<div class="vc_col-sm-4 wpb_column vc_column_container">
									<div class="wpb_wrapper">
									<?php the_field('donate_twt_address'); ?>
									<img src="<?php the_field('donate_image'); ?>" />
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
					<?php
					if($enable_page_comments){
					?>
					<div class="container">
						<div class="container_inner">
					<?php
						comments_template('', true); 
					?>
						</div>
					</div>	
					<?php
					}
					?> 
					<?php endwhile; ?>
				<?php endif; ?>
		<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
			
			<?php if($sidebar == "1") : ?>	
				<div class="two_columns_66_33 clearfix grid2">
					<div class="column1">
			<?php elseif($sidebar == "2") : ?>	
				<div class="two_columns_75_25 clearfix grid2">
					<div class="column1">
			<?php endif; ?>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<div class="column_inner">
						
						<?php the_content(); ?>	
						<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
						</div>
				<?php endwhile; ?>
				<?php endif; ?>
			
							
					</div>
					<div class="column2"><?php get_sidebar();?></div>
				</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
				<?php if($sidebar == "3") : ?>	
					<div class="two_columns_33_66 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php elseif($sidebar == "4") : ?>	
					<div class="two_columns_25_75 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php endif; ?>
						<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<div class="column_inner">
							<?php the_content(); ?>		
							<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
							</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
								
						</div>
						
					</div>
			<?php endif; ?>
	</div>
	</div>	
	<?php get_footer(); ?>