<?php 
/*
Template Name: Reports Page
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider"><div class="q_slider_inner">
			<?php echo do_shortcode($revslider); ?>
			</div></div>
		<?php
		}
		?>
	<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
	<div class="full_width_inner">
		<?php if(($sidebar == "default")||($sidebar == "")) : ?>
			<?php if (have_posts()) : 
					while (have_posts()) : the_post(); ?>

					<div style="background-color:#ffffff; padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section" data-q_icon="fa-list" data-q_title="Portfolio List" data-q_id="#portfolio">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">

												<!-- Reports Information and Link to Brochure -->

												<div class="vc_col-sm-6 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('reports_overview'); ?>
														<a class="primary_button" href="<?php the_field('reports_brochure_file'); ?>" target="_blank"><?php the_field('reports_brochure_name'); ?></a>
													</div> 
												</div> 

												<!-- Reports Key Statistics -->

												<div class="vc_col-sm-6 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="reports_container">
															<div class="reports_top">
																<h2>
																	<?php													
																		$count_projects = wp_count_posts('projects');
																		$actual_projects = $count_projects->publish + 7;
																		echo $actual_projects;
																	?>
																</h2>
																<p>Completed Projects</p>
															</div>
															<div class="reports_bottom">
																<div class="reports_bottom_text">
																	<p>Benefiting</p>
																	<h2>
																		<!-- Count amount of school projects and lives impacted -->

																		<?php
																			$schoolargs = array('post_type' => 'projects', 'post_status' => 'publish', 'post_parent' => 732);
																			$schoolquery = new WP_Query($schoolargs);
																			$schoollives = ($schoolquery->found_posts - 14) * 800;
																		?>

																		<!-- Count amount of village projects and lives impacted -->

																		<?php
																			$villageargs = array('post_type' => 'projects', 'post_status' => 'publish', 'post_parent' => 613);
																			$villagequery = new WP_Query($villageargs);
																			$villagelives = ($villagequery->found_posts - 206) * 250;
																		?>

																		<!-- Count total amount of lives impacted -->

																		<?php
																			$totallives = $schoollives + $villagelives + 73000;
																			$people = number_format($totallives);
																			echo $people;
																		?>
																	</h2>
																	<p>People</p>
																</div>
																<div class="reports_bottom_text">
																	<p style="word-spacing: 3px">Of Which</p>
																	<h2>
																		<?php
																			$kids = $totallives * .567;
																			echo number_format($kids, 0, '.', ',');
																		?>
																	</h2>
																	<p>Are Kids*</p>
																</div>
															</div>
														</div>
														<p style="text-align: center; margin-top: 10px; font-size: 12px;"><a href="http://dhsprogram.com/pubs/pdf/FR264/FR264.pdf" target="_blank">*Uganda Bureau of Statistics. Demographic and Health Survey, 2011.</a></p>
													</div> 
												</div> 
											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>
					<div style="padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section subtle_background_pattern">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">

												<div class="vc_col-sm-12 wpb_column vc_column_container">
													<div class="wpb_wrapper">
													<h2 class="secondary_header">Recent Reports</h2>
													</div> 
												</div> 

												<!-- Field Reports -->

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<h2>Field</h2>
														<?php
														$rowsarray = get_field('fieldreports');
														$rows = array_reverse($rowsarray);
														if($rows)
														{
															$i = 0;

															echo '<ul class="reports_list">';

															foreach($rows as $row)
															{
																$i++;
																if ($i > 3):
																	break;
																endif;
																echo '<li>' . '<a href=' .$row['field_report_file']. ' target="_blank"> ' . $row['field_report_name'] . '</a>' . '</li>';
															}

															echo '</ul>';
														}

														?>
													</div> 
												</div> 

												<!-- Annual Reports -->

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<h2>Annual</h2>
														<?php
														$rowsarray = get_field('annualreports');
														$rows = array_reverse($rowsarray);
														if($rows)
														{
															$i = 0;

															echo '<ul class="reports_list">';

															foreach($rows as $row)
															{
																$i++;
																if ($i > 3):
																	break;
																endif;
																echo '<li>' . '<a href=' .$row['annual_report_file']. ' target="_blank"> ' . $row['annual_report_name'] . '</a>' . '</li>';
															}

															echo '</ul>';
														}

														?>
													</div> 
												</div> 

												<!-- Financial Reports -->

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<h2>Financial</h2>
														<?php
														$rowsarray = get_field('financialreports');
														$rows = array_reverse($rowsarray);
														if($rows)
														{
															$i = 0;

															echo '<ul class="reports_list">';

															foreach($rows as $row)
															{
																$i++;
																if ($i > 3):
																	break;
																endif;
																echo '<li>' . '<a href=' .$row['financial_report_file']. ' target="_blank"> ' . $row['financial_report_name'] . '</a>' . '</li>';
															}

															echo '</ul>';
														}

														?>
													</div> 
												</div> 

												<!-- Note About Reports posted prior to April 2012 --> 

												<div class="vc_col-sm-12 wpb_column vc_column_container">
													<div class="wpb_wrapper center">
													<a href="https://watertrust.org/all-reports/" class="secondary_button">View All Reports</a>
													</div> 
												</div> 

											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
					<?php
					if($enable_page_comments){
					?>
					<div class="container">
						<div class="container_inner">
					<?php
						comments_template('', true); 
					?>
						</div>
					</div>	
					<?php
					}
					?> 
					<?php endwhile; ?>
				<?php endif; ?>
		<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
			
			<?php if($sidebar == "1") : ?>	
				<div class="two_columns_66_33 clearfix grid2">
					<div class="column1">
			<?php elseif($sidebar == "2") : ?>	
				<div class="two_columns_75_25 clearfix grid2">
					<div class="column1">
			<?php endif; ?>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<div class="column_inner">
						
						<?php the_content(); ?>	
						<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
						</div>
				<?php endwhile; ?>
				<?php endif; ?>
			
							
					</div>
					<div class="column2"><?php get_sidebar();?></div>
				</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
				<?php if($sidebar == "3") : ?>	
					<div class="two_columns_33_66 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php elseif($sidebar == "4") : ?>	
					<div class="two_columns_25_75 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php endif; ?>
						<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<div class="column_inner">
							<?php the_content(); ?>		
							<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
							</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
								
						</div>
						
					</div>
			<?php endif; ?>
	</div>
	</div>	
	<?php get_footer(); ?>