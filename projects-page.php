<?php 
/*
Template Name: Projects Page
*/ 
?>

<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider">
				<div class="q_slider_inner">
					<?php echo do_shortcode($revslider); ?>
				</div>
			</div>
		<?php
		}
		?>
		<div class="container"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
			<div class="container_inner default_template_holder clearfix page_container_inner">
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>
					<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>

							<!-- mWater Interactive Map -->

							<div style="padding-top:0px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section">
								<div class=" section_inner clearfix">
									<div class="section_inner_margin clearfix">
										<div class="vc_col-sm-12 wpb_column vc_column_container">
											<div class="wpb_wrapper">

												<!-- Map -->

												<div id="map" style="width: 100%; height: 480px;"></div>

												<!-- Legend -->

												<div class="legend_container_projects">
													<div class="legend">
														<div class="legend-item">
															<div class="legend-box high"></div>
															<div class="legend-text">Functional</div>
														</div>
														<div class="legend-item">
															<div class="legend-box basic"></div>
															<div class="legend-text">Partially Functional</div>
														</div>
														<div class="legend-item">
															<div class="legend-box inadequate"></div>
															<div class="legend-text">Not Functional</div>
														</div>
														<div class="legend-item">
															<div class="legend-box nodata"></div>
															<div class="legend-text">No Data</div>
														</div>
													</div>

													<!-- CSV Dump -->

													<a href="https://api.mwater.co/v3/queries/twt-sites-indicators.csv" class="legend-data">Download a CSV of collected indicators</a>
												</div>
												<p>We are committed to long-lasting change and to get there, we measure key performance indicators of all our projects.  Read about them <a href="http://watertrust.org/key-performance-indicators/">here</a>.  See them <a href="http://watertrust.org/our-projects/">here</a>. We are committed to clean, safe water.  To get there, we will do <a href="http://watertrust.org/our-commitment-to-clean-water/">this</a>.</p>
												<script>
												jQuery(function() {
													window.displayMap(jQuery("#map").get(0), {imagePath: "../wp-content/themes/thewatertrust-child/images/"});
												 })
												</script>
											</div> 
										</div> 
									</div>
								</div>
							</div>

							<div style="padding-top:0px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section">
								<div class=" section_inner clearfix">
									<div class="section_inner_margin clearfix">
										<div class="vc_col-sm-4 wpb_column vc_column_container">
											<div class="wpb_wrapper">

												<!-- Featured a Project Dropdowns -->

												<h2 class="secondary_header">Find a Project</h2>

												<!-- Find a Project by Village -->

												<form action="<?php bloginfo('url'); ?>" method="get">
													<?php
													$select = wp_dropdown_pages(
														array(
															'post_type'        => 'projects',
															'child_of'         => 613,
															'show_option_none' => 'Villages',
															'echo' 			   => 0
															)
														);

													echo str_replace('<select ', '<select onchange="this.form.submit()" ', $select);
													?>
												</form>

												<!-- Find a Project by School -->

												<form action="<?php bloginfo('url'); ?>" method="get">
													<?php
													$select = wp_dropdown_pages(
														array(
															'post_type'        => 'projects',
															'child_of'         => 732,
															'show_option_none' => 'Schools',
															'echo' 			   => 0
															)
														);

													echo str_replace('<select ', '<select onchange="this.form.submit()" ', $select);
													?>
												</form>

												<!-- Find a Health Center -->

												<form action="<?php bloginfo('url'); ?>" method="get">
													<?php
													$select = wp_dropdown_pages(
														array(
															'post_type'        => 'projects',
															'child_of'         => 20686,
															'show_option_none' => 'Health Centers',
															'echo' 			   => 0
															)
														);

													echo str_replace('<select ', '<select onchange="this.form.submit()" ', $select);
													?>
												</form>

												<!-- Find a Project by Under Construction -->

												<form action="<?php bloginfo('url'); ?>" method="get">
													<?php
													$select = wp_dropdown_pages(
														array(
															'post_type'        => 'projects',
															'child_of'         => 615,
															'show_option_none' => 'Under Construction',
															'echo' 			   => 0
															)
														);

													echo str_replace('<select ', '<select onchange="this.form.submit()" ', $select);
													?>
												</form>

												<!-- Search for a Project -->

												<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
													<input type="text" placeholder="Type donor name" value="<?php the_search_query(); ?>" name="s" id="s" />
													<input type="submit" id="searchsubmit" value="Search" />
												</form>

											</div> 
										</div>

										<!-- Featured Projects -->

										<div class="vc_col-sm-8 wpb_column vc_column_container">
											<div class="wpb_wrapper">
												<h2 class="secondary_header">Featured Projects</h2>
												<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
													<div class=" full_section_inner clearfix">
														<?php
														$args = array(
															'post_type'           => 'projects',
															'project_category'    => 'project-page-featured',
															'orderby'             => 'rand',
															'posts_per_page'      => 4

															);
														$featured_project = new WP_Query( $args );
														?>
														
														<?php 
														$i = 1;
														echo '<div class="vc_row">';
														if( $featured_project->have_posts() ) : while( $featured_project->have_posts() ) : $featured_project->the_post(); ?>
															<div class="vc_col-sm-6 wpb_column vc_column_container">
																<div class="featured_project_container">
																	<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
																	<div class="featured_project_info">
																		<h3><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></h3>
																		<p><b>Donor:</b> <?php the_field('donor_name', $post->ID);?></p>
																		<a href="<?php echo get_permalink(); ?>">View Project</a>
																	</div>
																</div>
															</div>
														<?php if($i % 2 == 0) {echo '</div><div class="vc_row">';} ?>
														<?php $i++; endwhile; else: ?>
														<?php endif; ?>
														</div>
															
													</div>
												</div>
											</div> 
										</div> 
									</div>
								</div>
							</div>

							<?php 
								$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
								);
								wp_link_pages($args_pages);
							?>
							<?php
							if($enable_page_comments){
								comments_template('', true); 
							}
							?> 
							<?php endwhile; ?>
						<?php endif; ?>
				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
					
					<?php if($sidebar == "1") : ?>	
						<div class="two_columns_66_33 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php elseif($sidebar == "2") : ?>	
						<div class="two_columns_75_25 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php endif; ?>
							<?php if (have_posts()) : 
								while (have_posts()) : the_post(); ?>
								<div class="column_inner">
								
								<?php the_content(); ?>
								<?php 
									$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
									);

									wp_link_pages($args_pages);
								?>
								<?php
								if($enable_page_comments){
									comments_template('', true); 
								}
								?> 
								</div>
						<?php endwhile; ?>
						<?php endif; ?>
					
									
							</div>
							<div class="column2"><?php get_sidebar();?></div>
						</div>
					<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
						<?php if($sidebar == "3") : ?>	
							<div class="two_columns_33_66 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php elseif($sidebar == "4") : ?>	
							<div class="two_columns_25_75 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php endif; ?>
								<?php if (have_posts()) : 
									while (have_posts()) : the_post(); ?>
									<div class="column_inner">
										<?php the_content(); ?>
										<?php 
											$args_pages = array(
												'before'           => '<p class="single_links_pages">',
												'after'            => '</p>',
												'pagelink'         => '<span>%</span>'
											);
											wp_link_pages($args_pages);
										?>
										<?php
										if($enable_page_comments){
											comments_template('', true); 
										}
										?> 
									</div>
							<?php endwhile; ?>
							<?php endif; ?>
						
										
								</div>
								
							</div>
					<?php endif; ?>
			
		</div>
	</div>
	<?php get_footer(); ?>