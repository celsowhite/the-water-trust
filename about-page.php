<?php 
/*
Template Name: About Page
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider"><div class="q_slider_inner">
			<?php echo do_shortcode($revslider); ?>
			</div></div>
		<?php
		}
		?>
	<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
	<div class="full_width_inner">
		<?php if(($sidebar == "default")||($sidebar == "")) : ?>
			<?php if (have_posts()) : 
					while (have_posts()) : the_post(); ?>

					<!-- Overview -->

					<div style="background-color:#ffffff; padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section" data-q_icon="fa-list" data-q_title="Portfolio List" data-q_id="#portfolio">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-6 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<?php the_field('about_overview_description'); ?>
													</div> 
												</div> 

												<div class="vc_col-sm-6 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="wpb_video_widget wpb_content_element video_player video_player">
															<div class="wpb_wrapper">
																<div class="wpb_video_wrapper">
																	<div style="background-image: url(//i.vimeocdn.com/video/483668044_1280.jpg);" itemtype="http://schema.org/VideoObject" itemscope="" class="arve-wrapper arve-normal-wrapper arve-vimeo-wrapper ">
																		<div class="arve-embed-container">
																			<iframe frameborder="0" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" scrolling="no" data-src="//player.vimeo.com/video/101794624?autoplay=1" class="arve-inner arve-hidden" id="arve-iframe-1"></iframe><button data-target="arve-iframe-1" class="arve-inner arve-play-background arve-iframe-btn"></button>
																		</div>
																	</div>
																	<a class="arve-hidden" href="http://vimeo.com/101794624">http://vimeo.com/101794624</a>
																</div>
															</div> 
														</div> 
													</div> 
												</div> 
											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<!-- Middle Quote -->

					<div style="background-image:url(<?php the_field('about_quote_background_image'); ?>); padding-top:120px; padding-bottom:120px; text-align:center;" class="vc_row wpb_row section vc_row-fluid background_image_top grid_section">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<h2 class="header_over_image"><?php the_field('about_quote'); ?></h2>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<!-- Services -->

					<div style="padding-top:50px; padding-bottom:20px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section subtle_background_pattern">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">

										<div class="header_with_subtitle">
											<h2><?php the_field('services_title'); ?></h2>
											<?php the_field('services_description'); ?>
										</div>

										<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="twt_icon_with_title">
															<div class="twt_icon_holder">
																<img src="<?php the_field('hand-dug_wells_image'); ?>" />
															</div>
															<div class="twt_icon_text_holder">
																<div class="twt_icon_text_inner">
																	<h5><?php the_field('hand-dug_wells_title'); ?></h5>
																	<p><?php the_field('hand-dug_wells_description'); ?></p>
																</div>
															</div>
														</div>
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="twt_icon_with_title">
															<div class="twt_icon_holder">
																<img src="<?php the_field('drill_borehole_wells_image'); ?>" />
															</div>
															<div class="twt_icon_text_holder">
																<div class="twt_icon_text_inner">
																	<h5><?php the_field('drill_borehole_wells_title'); ?></h5>
																	<p><?php the_field('drill_borehole_wells_description'); ?></p>
																</div>
															</div>
														</div>
													</div> 
												</div> 

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="twt_icon_with_title">
															<div class="twt_icon_holder">
																<img src="<?php the_field('build_latrines_image'); ?>" />
															</div>
															<div class="twt_icon_text_holder">
																<div class="twt_icon_text_inner">
																	<h5><?php the_field('build_latrines_title'); ?></h5>
																	<p><?php the_field('build_latrines_description'); ?></p>
																</div>
															</div>
														</div>
													</div> 
												</div> 
											</div>
										</div>

										<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												
												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="twt_icon_with_title">
															<div class="twt_icon_holder">
																<img src="<?php the_field('household_water_treatments_image'); ?>" />
															</div>
															<div class="twt_icon_text_holder">
																<div class="twt_icon_text_inner">
																	<h5><?php the_field('household_water_treatments_title'); ?></h5>
																	<p><?php the_field('household_water_treatments_description'); ?></p>
																</div>
															</div>
														</div>
													</div> 
												</div>  

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="twt_icon_with_title">
															<div class="twt_icon_holder">
																<img src="<?php the_field('harvest_rainwater_image'); ?>" />
															</div>
															<div class="twt_icon_text_holder">
																<div class="twt_icon_text_inner">
																	<h5><?php the_field('harvest_rainwater_title'); ?></h5>
																	<p><?php the_field('harvest_rainwater_description'); ?></p>
																</div>
															</div>
														</div>
													</div> 
												</div>  

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="twt_icon_with_title">
															<div class="twt_icon_holder">
																<img src="<?php the_field('protect_springs_image'); ?>" />
															</div>
															<div class="twt_icon_text_holder">
																<div class="twt_icon_text_inner">
																	<h5><?php the_field('protect_springs_title'); ?></h5>
																	<p><?php the_field('protect_springs_description'); ?></p>
																</div>
															</div>
														</div>
													</div> 
												</div>  
											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<!-- Performance Indicators -->

					<div style="background-color:#ffffff; padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section" data-q_icon="fa-list" data-q_title="Portfolio List" data-q_id="#portfolio">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">

										<div class="header_with_subtitle">
											<h2><?php the_field('performance_indicators_title'); ?></h2>
											<p><?php the_field('performance_indicators_description'); ?></p>
										</div>

										<div style=" text-align:center;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">

												<!-- Latrine Use Indicator -->

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div id="third_pie" style="width: 100%;"></div>
														<?php the_field('latrine_use_indicator'); ?>
													</div> 
												</div> 

												<!-- Functionality Indicator -->

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div id="second_pie" style="width: 100%;"></div>
														<?php the_field('functionality_indicator'); ?>
													</div> 
												</div> 

												<!-- Water Quality Indicator -->

												<div class="vc_col-sm-4 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div id="first_pie" style="width: 100%;"></div>
														<?php the_field('water_quality_indicator'); ?>
													</div> 
												</div> 

												<script>
												jQuery(function() {
													window.displayIndicatorDonut([
														[jQuery("#first_pie").get(0), "water_quality", false]
														],
														[
														["Inadequate: Water quality test fails to meet National standards or sanitary inspection is poor", '#939598'],						
														['Country Standard: Meets National rural water standards for quality and sanitary inspection', '#68C5BB'],
														['High:', ''],
														["No Data", '#DCDDDE']
														]
														);
													window.displayIndicatorDonut([
														[jQuery("#second_pie").get(0), "functionality", false]
														],
														[
														["Inadequate: Not Functional", '#939598'],
														['Basic: Partially functional', '#272962'],
														['High: Functional', '#378BCA'],
														["No Data", '#DCDDDE']
														]
														);
													window.displayIndicatorDonut([
														[jQuery("#third_pie").get(0), "uses_latrine", false]
														],
														[
														["Inadequate: No latrine or pit", '#939598'],
														['Basic: Pit', '#272962'],
														['High: Improved latrine or latrine', '#378BCA'],
														["No Data", '#DCDDDE']
														]
														);
												})

												</script>

											</div>
										</div>
										<div style=" text-align:center;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-12 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<div class="legend_container">
														<div class="about_legend legend">
															<div class="legend-item">
																<div class="legend-box high"></div>
																<div class="legend-text">High</div>
															</div>
															<div class="legend-item">
																<div class="legend-box basic"></div>
																<div class="legend-text">Basic</div>
															</div>
															<div class="legend-item">
																<div class="legend-box inadequate"></div>
																<div class="legend-text">Inadequate</div>
															</div>
															<div class="legend-item">
																<div class="legend-box nodata"></div>
																<div class="legend-text">No Data</div>
															</div>
															<div class="legend-item">
																<div class="legend-box country_standard"></div>
																<div class="legend-text">National Standard</div>
															</div>
														</div>
														</div>
													</div> 
												</div> 
											</div>
										</div>
										<div style=" text-align:center;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">
												<div class="vc_col-sm-12 wpb_column vc_column_container">
													<div class="wpb_wrapper">
														<p>We are committed to long-lasting change and to get there, we measure key performance indicators of all our projects.  Read about them <a href="http://watertrust.org/key-performance-indicators/">here</a>.  See them <a href="http://watertrust.org/our-projects/">here</a>. We are committed to clean, safe water.  To get there, we will do <a href="http://watertrust.org/our-commitment-to-clean-water/">this</a>.</p>

														</div>
													</div> 
												</div> 
											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<div style="background-image:url(https://watertrust.org/wp-content/uploads/2014/10/J62A4682.jpg); padding-top:80px; padding-bottom:70px; text-align:center;" class="vc_row wpb_row section vc_row-fluid grid_section">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<h2 class="header_over_image">Want to see our performance indicators in action?</h2>
										<a class="tertiary_button" href="<?php echo wp_make_link_relative('https://watertrust.org/our-projects/'); ?>">View Our Projects</a>												
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
					<?php
					if($enable_page_comments){
					?>
					<div class="container">
						<div class="container_inner">
					<?php
						comments_template('', true); 
					?>
						</div>
					</div>	
					<?php
					}
					?> 
					<?php endwhile; ?>
				<?php endif; ?>
		<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
			
			<?php if($sidebar == "1") : ?>	
				<div class="two_columns_66_33 clearfix grid2">
					<div class="column1">
			<?php elseif($sidebar == "2") : ?>	
				<div class="two_columns_75_25 clearfix grid2">
					<div class="column1">
			<?php endif; ?>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<div class="column_inner">
						
						<?php the_content(); ?>	
						<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
						</div>
				<?php endwhile; ?>
				<?php endif; ?>
			
							
					</div>
					<div class="column2"><?php get_sidebar();?></div>
				</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
				<?php if($sidebar == "3") : ?>	
					<div class="two_columns_33_66 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php elseif($sidebar == "4") : ?>	
					<div class="two_columns_25_75 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php endif; ?>
						<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<div class="column_inner">
							<?php the_content(); ?>		
							<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
							</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
								
						</div>
						
					</div>
			<?php endif; ?>
	</div>
	</div>	
	<?php get_footer(); ?>