<?php 
/*
Template Name: Homepage
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider"><div class="q_slider_inner">
			<?php echo do_shortcode($revslider); ?>
			</div></div>
		<?php
		}
		?>
	<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
	<div class="full_width_inner">
		<?php if(($sidebar == "default")||($sidebar == "")) : ?>
			<?php if (have_posts()) : 
					while (have_posts()) : the_post(); ?>

					<!-- What Makes Us Unique -->

					<div style="padding-top:50px; padding-bottom:50px; text-align:center;" class="vc_row wpb_row section vc_row-fluid grid_section subtle_background_pattern">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<h2 class="main_header"><?php the_field('attributes_title'); ?></h2>
								<div class="vc_col-sm-6 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<!--<img src="<?php the_field('attribute_1_image'); ?>" />-->
										<h3><?php the_field('attribute_1_name') ?></h3>
										<p><?php the_field('attribute_1_description') ?></p>
									</div> 
								</div> 

								<div class="vc_col-sm-6 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<!--<img src="<?php the_field('attribute_2_image'); ?>" />-->
										<h3><?php the_field('attribute_2_name') ?></h3>
										<p><?php the_field('attribute_2_description') ?></p>
									</div> 
								</div> 

								<div class="vc_col-sm-6 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<!--<img src="<?php the_field('attribute_3_image'); ?>" />-->
										<h3><?php the_field('attribute_3_name') ?></h3>
										<p><?php the_field('attribute_3_description') ?></p>
									</div> 
								</div> 

								<div class="vc_col-sm-6 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<!--<img src="<?php the_field('attribute_4_image'); ?>" />-->
										<h3><?php the_field('attribute_4_name') ?></h3>
										<p><?php the_field('attribute_4_description') ?></p>
									</div> 
								</div> 
								<a href="https://watertrust.org/our-approach/" class="secondary_button">Learn about our approach</a>
							</div>
						</div>
					</div>

					<!-- The Water Trust Video -->

					<div id="twt_video" style=" padding-top:50px; padding-bottom:50px; text-align:center;" class="vc_row wpb_row section vc_row-fluid grid_section">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<h2 class="main_header">Experience The Water Trust</h2>
										<div class="wpb_video_widget wpb_content_element video_player video_player">
											<div class="wpb_wrapper">
												<div class="wpb_video_wrapper">
													<div style="background-image: url(http://i.vimeocdn.com/video/483668044_1280.jpg);" itemtype="http://schema.org/VideoObject" itemscope="" class="arve-wrapper arve-normal-wrapper arve-vimeo-wrapper ">
														<div class="arve-embed-container">
															<iframe frameborder="0" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" scrolling="no" data-src="//player.vimeo.com/video/101794624?autoplay=1" class="arve-inner arve-hidden" id="arve-iframe-1"></iframe><button data-target="arve-iframe-1" class="arve-inner arve-play-background arve-iframe-btn"></button>
														</div>
													</div>
													<a class="arve-hidden" href="http://vimeo.com/101794624">http://vimeo.com/101794624</a>
												</div>
											</div> 
										</div> 
									</div> 
								</div> 
								<a href="https://watertrust.org/our-projects/" class="primary_button">View Our Projects</a>
							</div>
						</div>
					</div>

					<!-- Bottom Featured Content -->

					<div style="padding-top:50px; padding-bottom:50px; text-align:left;" class="vc_row wpb_row section vc_row-fluid grid_section subtle_background_pattern">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<div style=" text-align:left;" class="vc_row wpb_row section vc_row-fluid">
											<div class=" full_section_inner clearfix">

												<h2 class="secondary_header">News</h2>

												<div class="twt_row">

													<!-- Featured News 1 -->

													<div class="column_1_3 stretch_height">
														<?php
														$post_object = get_field('news_block_1');
														if( $post_object ): 
														$post = $post_object;
														setup_postdata( $post ); 
														?>
														    <div class="homepage_featured_box">
														    	<?php if(has_post_thumbnail()): ?>
																	<div class="homepage_featured_image" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
																<?php endif; ?>
																<div class="homepage_featured_text">
																	<h3><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></h3>
																	<p><?php echo get_excerpt(150); ?></p>
																	<a href="<?php echo get_permalink(); ?>">View Post</a>
																</div>

															</div>
														    <?php wp_reset_postdata(); ?>
														<?php endif; ?>
													</div>

													<!-- Featured News 2 -->

													<div class="column_1_3 stretch_height">
														<?php
														$post_object = get_field('news_block_2');
														if( $post_object ): 
														$post = $post_object;
														setup_postdata( $post ); 
														?>
														    <div class="homepage_featured_box">

																<?php if(has_post_thumbnail()): ?>
																	<div class="homepage_featured_image" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
																<?php endif; ?>
																<div class="homepage_featured_text">
																	<h3><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></h3>
																	<p><?php echo get_excerpt(150); ?></p>
																	<a href="<?php echo get_permalink(); ?>">View Post</a>
																</div>

															</div>
														    <?php wp_reset_postdata(); ?>
														<?php endif; ?>
													</div>

													<!-- Featured News 3 -->

													<div class="column_1_3 stretch_height">
														<?php
														$post_object = get_field('news_block_3');
														if( $post_object ): 
														$post = $post_object;
														setup_postdata( $post ); 
														?>
														    <div class="homepage_featured_box">

																<?php if(has_post_thumbnail()): ?>
																	<div class="homepage_featured_image" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
																<?php endif; ?>
																<div class="homepage_featured_text">
																	<h3><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></h3>
																	<p><?php echo get_excerpt(150); ?></p>
																	<a href="<?php echo get_permalink(); ?>">View Post</a>
																</div>

															</div>
														    <?php wp_reset_postdata(); ?>
														<?php endif; ?>
													</div>
												</div>
											</div>
										</div>
									</div> 
								</div> 
							</div>
						</div>
					</div>

					<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
					<?php
					if($enable_page_comments){
					?>
					<div class="container">
						<div class="container_inner">
					<?php
						comments_template('', true); 
					?>
						</div>
					</div>	
					<?php
					}
					?> 
					<?php endwhile; ?>
				<?php endif; ?>
		<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
			
			<?php if($sidebar == "1") : ?>	
				<div class="two_columns_66_33 clearfix grid2">
					<div class="column1">
			<?php elseif($sidebar == "2") : ?>	
				<div class="two_columns_75_25 clearfix grid2">
					<div class="column1">
			<?php endif; ?>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<div class="column_inner">
						
						<?php the_content(); ?>	
						<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
						</div>
				<?php endwhile; ?>
				<?php endif; ?>
			
							
					</div>
					<div class="column2"><?php get_sidebar();?></div>
				</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
				<?php if($sidebar == "3") : ?>	
					<div class="two_columns_33_66 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php elseif($sidebar == "4") : ?>	
					<div class="two_columns_25_75 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php endif; ?>
						<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<div class="column_inner">
							<?php the_content(); ?>		
							<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
							</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
								
						</div>
						
					</div>
			<?php endif; ?>
	</div>
	</div>	
	<?php get_footer(); ?>